# How to add custom platforms into RetroFE for RG Nano/Funkey devices
## Example for the fake-08 (Pico-8) core

1. Add roms and boxarts
`/mnt/Pico-8`

2. Import the fake08 core
`/rootfs/usr/games/cores/fake08_libretro.so`

3. Copy /Libretro/ collection folder for RetroRoomCovers theme
`/rootfs/usr/games/layouts/RetroRoomCovers/collections/Pico-8`

4. Update the menu artwork to match Pico-8 logo
`/rootfs/usr/games/layouts/RetroRoomCovers/collections/Pico-8/system_artwork/device_W240.png`

5. Copy /Libretro/ collection folder for RetroFE
`/rootfs/usr/games/collections/Pico-8`

6. Update settings.conf;
```
# /rootfs/usr/games/collections/Pico-8/settings.conf

list.path = %BASE_ITEM_PATH%/%ITEM_COLLECTION_NAME%
list.includeMissingItems = false
list.extensions = p8,P8
list.menuSort = yes
list.romHierarchy = true
launcher = pico-8
media.artwork_front   = %BASE_ITEM_PATH%/%ITEM_COLLECTION_NAME%
```

7. Update platforms list for RetroFE;
```
# /rootfs/usr/games/collections/Main/menu.txt

Game Boy
Game Boy Color
Game Gear
Neo Geo Pocket
NES
Pico-8
```

8. Add the ROM launcher;
```
# /rootfs/usr/games/launchers/pico-8.conf

executable = %RETROFE_PATH%/launchers/pico-8_launch.sh
arguments = "%ITEM_FILEPATH%"
```

```
# /rootfs/usr/games/launchers/pico-8_launch.sh

#!/bin/sh
picoarch /rootfs/usr/games/cores/fake08_libretro.so "$1" &
pid record $!
wait $!
pid erase
```

9. Post-install configuration
```
Launch a game and press the menu button
Advanced > Options > Audio and Video > Set "Screen size" to "Aspect"
Go back to previous menu
Save config > Save global config
```
